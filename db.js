'use strict';

const mongoose = require('mongoose');
const log = require('bole')('mongo');
const Promise = require("bluebird");

mongoose.Promise = Promise;

var config = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_DATABASE
};

var connect;
var fail;
mongoose.EnsureInitialized = new Promise(function (resolve, reject) {
    connect = resolve;
    fail = reject;
});

mongoose.connect('mongodb://' + config.host + ':' + config.port + '/' + config.database);
var db = mongoose.connection;
db.on('error', function(err) {
    log.error("Mongo error: " + err);
    if (mongoose.EnsureInitialized.isPending()) {
        fail(err);
    }
});
db.once('open', function() {
    log.info('Mongo connected.');
    connect(mongoose);
});

module.exports = mongoose;
