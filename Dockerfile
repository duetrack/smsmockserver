FROM node:5.4.1-onbuild
ENV HOST="0.0.0.0" \
	PORT="80" \
	DB_HOST="db" \
	DB_PORT="27017"
EXPOSE 1337
