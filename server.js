'use strict';

require('dotenv').load();
var Promise = require('bluebird');

var bole = require('bole');
var log = bole('server');
bole.output({
    level: process.env.NODE_ENV === "development" ? 'debug' : 'info',
    stream: process.stdout
});

const mongoose = require('./db.js');
const server = require('./hapiserver.js');

Promise.all([
    mongoose.EnsureInitialized,
    server.EnsureInitialized
]).then(() => {
    var schema = new mongoose.Schema({
        date: { type: Date, default: Date.now },
        phone: { type: String, required: true },
        message: { type: String, required: true }
    });
    return mongoose.model('Message', schema);
}).then((model) => {
    server.route([
        {
            method: 'GET',
            path: '/',
            handler: {
                file: 'index-page.html'
            }
        }, {
            method: 'GET',
            path: '/api/v1/message',
            handler: (request, reply) => {
                return reply(model.find());
            }
        }, {
            method: 'POST',
            path: '/api/v1/message',
            handler: (request, reply) => {
                request._id = null;
                request.__v = null;
                return reply(model.create(request.payload));
            }
        }, {
            method: 'PURGE',
            path: '/api/v1/message',
            handler: (request, reply) => {
                return reply(model.remove());
            }
        }
    ]);
});




