'use strict';

const log = require('bole')('mongo');

const hapi = require('hapi');
const server = new hapi.Server();

var connect;
var fail;
server.EnsureInitialized = new Promise(function (resolve, reject) {
    connect = resolve;
    fail = reject;
});

server.connection({
    host: process.env.HOST || "localhost",
    port: process.env.PORT || "1337"
});

server.register(require('inert'), (err) => {
    if (err) {
        fail(err);
        return;
    }

    server.start((err) => {
        if (err) {
            fail(err);
            return;
        }

        log.debug('Server running at:', server.info.uri);
        connect(server);
    });
});

module.exports = exports = server;
